package com.elavon.tutorial.xmas;

public class TwelveDaysOfChristmas {

	public static void main(String[] args) {
		TwelveDaysOfChristmas.printLyrics();
	}

	 public static void printLyrics() {
	        
		 for(int i=1; i<=12; i++) {
			 System.out.print("On the ");
			 switch(i) {
			 case 1: System.out.println("1st day of Christmas, my true love sent to me:"); break;
			 case 2: System.out.println("2nd day of Christmas, my true love sent to me:"); break;
			 case 3: System.out.println("3rd day of Christmas, my true love sent to me:"); break;
			 default: System.out.println(i+"th day of Christmas, my true love sent to me:");
			 }
	        
			 switch(i) {
			 case 12: System.out.println("Twelve Drummers Drumming, ");
			 case 11: System.out.println("Eleven Pipers Piping, ");
			 case 10: System.out.println("Ten Lords a Leaping, ");
			 case 9: System.out.println("Nine Ladies Dancing, ");
			 case 8: System.out.println("Eight Maids a Milking, ");
			 case 7: System.out.println("Seven Swans a Swimming, ");
			 case 6: System.out.println("Six Geese a Laying, ");
			 case 5: System.out.println("Five Golden Rings,");
			 case 4: System.out.println("Four Calling Birds, ");
			 case 3: System.out.println("Three French Hens, ");
			 case 2: System.out.println("Two Turtle Doves \nand ");
			 case 1: System.out.println("A Partridge in a Pear Tree \n");
			 }
			 
		 }
	}
}
