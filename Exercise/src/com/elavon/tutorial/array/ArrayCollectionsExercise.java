package com.elavon.tutorial.array;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.HashMap;

public class ArrayCollectionsExercise {

     public static void main(String []args){
        String[] topCountries = {"South Korea", "Australia", "Japan", "USA", "Canada", "United Kingdom", "Greece", "Italy", "Spain", "France"};
        int sizeCountries = topCountries.length;
		System.out.println("Top Countries: " + sizeCountries);
		
		ArrayList<String> countryList = new ArrayList<String>();
		Collections.addAll(countryList, topCountries);
		System.out.println("List of Countries: " + countryList.size());
		countryList.add("Scotland");
		
		if (countryList.isEmpty()) {
		    System.out.println("List of Countries is empty");
		} else {
		    System.out.println("List of Countries is not empty");
		}
		
		int newSize = countryList.size();
		System.out.println("New size of List of Countries: " + newSize);
		
		if (countryList.contains("Mongolia")) {
		    System.out.println("List of Countries includes Mongolia");
		} else {
		    System.out.println("List of Countries does not include Mongolia");
		}
		
		if (countryList.contains("Scotland")) {
		    System.out.println("List of Countries includes Scotland");
		} else {
		    System.out.println("List of Countries does not include Scotland");
		}
		
		countryList.remove(10);
        
		ArrayList<String> topFiveCountries = new ArrayList<String>();
		topFiveCountries.add("Indonesia");
		topFiveCountries.add("United Kingdom");
		topFiveCountries.add("France");
		topFiveCountries.add("Italy");
		topFiveCountries.add("USA");
		
		for(int x = 0; x <=4; x++) {
			System.out.println(topFiveCountries.get(x) + " is on the list? " + countryList.contains(topFiveCountries.get(x)));
		}
		
        System.out.println(countryList);
        
        HashMap<Integer, String> countryMap = new HashMap<Integer, String>();
        countryMap.put(1, " South Korea");
        countryMap.put(2, " Australia");
        countryMap.put(3, " Japan");
        countryMap.put(4, " USA");
        countryMap.put(5, " Canada");
        countryMap.put(6, " United Kingdom");
        countryMap.put(7, " Greece");
        countryMap.put(8, " Italy");
        countryMap.put(9, " Spain");
        countryMap.put(10, " France");
        System.out.println(countryMap);


        Collections.sort(countryList, String.CASE_INSENSITIVE_ORDER);
        System.out.println(countryList);
        
        //18 Display your most loved countries by rank (using the Map of countries)
        
     }
     

}
