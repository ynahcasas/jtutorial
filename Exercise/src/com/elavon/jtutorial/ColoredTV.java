package com.elavon.jtutorial;

public class ColoredTV extends ExerciseBasicTV {
	protected int brightness=50;
	protected int contrast=50;
	protected int picture=50;
	int x = 10;
	
	ColoredTV(String brand, String model) {
		super(brand, model);
		this.brand = brand;
		this.model = model;
	}

	public int brightnessUp() {
		return this.brightness++;
	}
	
	public int brightnessDown() {
		return this.brightness--;
	}
	
	public int contrastUp() {
		return this.contrast++;
	}
	
	public int contrastDown() {
		return this.contrast--;
	}
	
	public int pictureUp() {
		return this.picture++;
	}
	
	public int pictureDown() {
		return this.picture--;
	}
	
	public int mute() {
		return volume = 0;
	}
	
	public int switchToChannel() {
		return channel = x;
	
	}
	public String toString() {
		return super.toString()+ "[b:"+ brightness+ " c:" + contrast + " p:"+ picture +"]";
	}
	
	public static void main(String[] args) {
		ExerciseBasicTV bnwTV = new ExerciseBasicTV("Admiral", "A1");
		ColoredTV sonyTV = new ColoredTV("Sony", "S1");
		
		System.out.println(bnwTV);
		System.out.println(sonyTV);
		sonyTV.brightnessUp();
		
		ColoredTV sharpTV = new ColoredTV("SHARP","SH1");
		sharpTV.mute();
		sharpTV.switchToChannel();
		sharpTV.brightnessDown();
		sharpTV.brightnessDown();
		sharpTV.pictureDown();
		sharpTV.contrastUp();
		System.out.println(sharpTV.toString());

	}

}
