package com.elavon.jtutorial;

public class ExerciseBasicTV {
	protected String brand;
	protected String model;
	protected boolean power = true;
	protected int channel = 0;
	protected int volume = 5;
	
	ExerciseBasicTV (String brand, String model) {
		this.brand = brand;
		this.model = model;
	}
	
	public String toString() {
		return brand + " " + model + " [ on: " + power + "," + "channel: "+ channel + "," + "volume: " + volume + " ]";
	}
	
	public boolean turnOn() {
		return power = true;
	}
	
	public boolean turnOff() {
		return power = false;
	}
	
	public int channelUp() {
		return this.channel++;
	}
	
	public int channelDown() {
		return this.channel--;
	}
	
	public int volumeUp() {
		return this.volume++;
	}
	
	public int volumeDown() {
		return this.volume--;
	}
	
	
	public static void main(String[] args) {
		int i;
		int x;
		
//Instantiate a TV object with a brand Andre Electronics and model ONE
		ExerciseBasicTV exerciseBasicTV = new ExerciseBasicTV("Andre Electronics", "ONE");

//		Print that object using System.out.println() and check if the default power, channel, volume printed are correct. The format you coded in toString() must be evident.
		System.out.println(exerciseBasicTV.toString());

//		Call the turnOn() method of the object created		
		exerciseBasicTV.turnOn();
		
//		Switch the channel up 5 times
//		Switch the channel down once
		for(i=0; i < 5; i++) {
			exerciseBasicTV.channelUp();
			if (i==4)
			{
				exerciseBasicTV.channelDown();
			}
		}

//		Turn the volume down 3 times
//		Turn the volume up once
		for(x=0; x < 3; x++) {
			exerciseBasicTV.volumeDown();
			if (x==2)
			{
				exerciseBasicTV.volumeUp();	
			}
		}
		
//		Turn off the TV object
		exerciseBasicTV.turnOff();
		
//		Print the object again and check if the TV is turned off, channel is 4, volume is 3
		System.out.println(exerciseBasicTV.toString());
	}

}
