package com.elavon.training.impl;

import com.elavon.training.intf.AwesomeCalculator;


public class AwesomeCalcImpl implements AwesomeCalculator {

	@Override
	public int getSum(int augend, int addend) {
		int getSum = augend + addend;
		return getSum;
	}

	@Override
	public double getDifference(double minuend, double subtrahend) {
		double getDifference = minuend - subtrahend;
		return getDifference;
	}

	@Override
	public double getProduct(double multiplicand, double multiplier) {
		double getProduct = multiplicand * multiplier;
		return getProduct;
	}

	@Override
	public String getQuotientAndRemainder(int dividend, int divisor) {
		int quotient = dividend / divisor;
		int remainder = dividend % divisor;
		String quotientStr = Integer.toString(quotient);
		String remainderStr = Integer.toString(remainder);
		String getQuotientAndRemainder = quotientStr + " remainder " + remainderStr;
		return getQuotientAndRemainder;
		
	}

	@Override
	public double toCelsius(int fahrenheit) {
		double toCelsius = (5 *(fahrenheit - 32)) / 9;
		return toCelsius;
	}

	@Override
	public double toFahrenheit(int celsius) {
		double toFahrenheit = celsius * 1.8 + 32;
		return toFahrenheit;
	}

	@Override
	public double toKilogram(double lbs) {
		double toKilogram = lbs * 0.4536;
		return toKilogram;
	}

	@Override
	public double toPound(double kg) {
		double toPound = kg * 2.2046;
		return toPound;
	}

	@Override
	public boolean isPalindrome(String str) {
		return str.equals(new StringBuilder(str).reverse().toString());
	}
}

