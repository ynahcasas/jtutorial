package com.elavon.training.main;

import com.elavon.training.impl.AwesomeCalcImpl;

public class Main {

	public static void main(String[] args) {
		AwesomeCalcImpl ans = new AwesomeCalcImpl();
		System.out.println("getSum:");
		System.out.println(ans.getSum(17,23));
		System.out.println(ans.getSum(0,5));
		
		System.out.println("getDifference:");
		System.out.println(ans.getDifference(5.9,10));
		System.out.println(ans.getDifference(50,15));
		
		System.out.println("getProduct:");
		System.out.println(ans.getProduct(3,20));
		System.out.println(ans.getProduct(3,0));
		
		System.out.println("getQuotientAndRemainder:");
		System.out.println(ans.getQuotientAndRemainder(17, 2));
		System.out.println(ans.getQuotientAndRemainder(10, 2));
		
		System.out.println("toCelsius:");
		System.out.println(ans.toCelsius(212));
		System.out.println(ans.toCelsius(10));
		
		System.out.println("toFahrenheit:");
		System.out.println(ans.toFahrenheit(100));
		System.out.println(ans.toFahrenheit(212));
		
		System.out.println("toKilogram:");
		System.out.println(ans.toKilogram(154));
		
		System.out.println("toPound:");
		System.out.println(ans.toPound(53.37));
		
		System.out.println("isPalindrome:");
		System.out.println(ans.isPalindrome("level"));
		System.out.println(ans.isPalindrome("river"));
	}


}
